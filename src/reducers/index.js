const defaultState = {say: 'GAY'}

const reducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'HELLO_REACT':
            return { ...state, say : action.payload  };
        default:
            return state;
    }

};

export default reducer;