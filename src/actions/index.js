//This file contatins 'ACTION-CREATOR' functions
//In Redux, 'ACTION-CREATOR'  return an  ACTION(plain JavaScript object)

//example of simple 'ACTION-CREATOR'

import axios from "axios/index";

export const sayHello = () => {
    return (dispatch) => {
        //dispatch({ type: 'HELLO_REACT'});
        axios.get('/crd-settings')
            .then(function(response){let resp1 = (JSON.stringify(response.data));
                dispatch({
                    type: 'HELLO_REACT',
                    payload: resp1
                })})
    }}