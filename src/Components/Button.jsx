import React, {Component} from 'react';
import axios from 'axios';
import {Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import { sayHello } from '../actions';

class Buttons extends Component {

    login() {

        axios.get('/crd-settings')
            .then(function(response){alert(JSON.stringify(response.data));})
    }

    render() {
        return (
            <div className="Button">
                <Button onClick={this.props.saySomething}>PRESS TO DISPATCH FIRST ACTION</Button>
                <h2>{this.props.whatsUp}</h2>
                <Button onClick={() => console.log('Redux State:',this.props.stateObject)} >Press to inspect STATE in console panel</Button>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    whatsUp: state.say,
    stateObject: state
})

const mapDispatchToProps = (dispatch) => ({
    saySomething: () => { dispatch(sayHello())}
})

Buttons = connect(
    mapStateToProps,
    mapDispatchToProps
)(Buttons)

export default Buttons;