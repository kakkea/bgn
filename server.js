var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var axios = require ('axios');
var app = express();
var srcPath = path.join(__dirname, '.');


app.use(express.static(srcPath));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));


//обрабтка гет запроса к хосту
app.get('/', (req, res) => {
    console.log(srcPath);
    res.sendFile('./build/index.html', {root: srcPath});
});


app.get('/crd-settings', (req,res) => {
    axios.get('http://yandex.ru')
        .then(function (response) {
            console.log(response.data);
            res.send('схема лимитов:420, лимиты 99999999');
        });
});

app.listen(3001, () => console.log('Example app listening on port 3001!'));

